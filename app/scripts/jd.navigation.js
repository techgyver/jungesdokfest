(function ( $, window, document, undefined ) {
    'use strict';
    var pluginName = 'jdNavigation',
        defaults = {
            propertyName: 'value'
        };

    function Plugin ( element, options ) {
        this.element = element;
        this.settings = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
        this._running = false;
    }

    Plugin.prototype = {
        init: function () {
            console.log('init nav-main');
            tmpl.render('#navigation-main-template', bunker.getStatic(), '.navigation-main');
            this.bindEvents();
            $('section').not('.current').hide();
        },
        bindEvents: function(){

            $('.navigation-main').on('click', 'a', function(e){
                e.preventDefault();
                location.hash = $(this).data('route');
            }).find('a').first().trigger('click');
                /*if (location.hash.length <= 1) {
                    $('.navigation-main').find('a').first().trigger('click');
                } else {
                    $(window).trigger('hashchange');
                }*/
        }
    };
    $.fn[ pluginName ] = function ( options ) {
        this.each(function() {
            if ( !$.data( this, 'plugin_' + pluginName ) ) {
                $.data( this, 'plugin_' + pluginName, new Plugin( this, options ) );
            }
        });
        return this;
    };

})( jQuery, window, document );