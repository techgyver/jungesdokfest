var programm = {
    "programm": [
        {
            "id": 1,
            "title": "Haltungsfragen",
            "prefix": "Programm 1",
            "screenings": [
                {
                    "id": 1,
                    "title": "Momentum",
                    "preps": [
                        {"dl": "a1.doc", "title":"name"}, {"dl":"a2.doc", "title":"name2"}
                    ],
                    "mats": [
                        {"dl": "a1.doc", "title":"Mname"}, {"dl":"a2.doc", "title":"Mname2"}
                    ]
                }, {
                    "id": 2,
                    "title": "The Duellists",
                    "preps": [
                        {"dl": "a1.doc", "title":"name"}, {"dl":"a2.doc", "title":"name2"}
                    ],
                    "mats": [
                        {"dl": "a1.doc", "title":"Mname"}, {"dl":"a2.doc", "title":"Mname2"}
                    ]
                }
            ]
        },
        {
            "id": 2,
            "title": " Zugehörigkeiten",
            "prefix": "Programm 2",
            "screenings": [
                {
                    "id": 1,
                    "title": "Body Trail",
                    "preps": [
                        {"dl": "1.doc", "title":"name"}, {"dl":"2.doc", "title":"name2"}
                    ],
                    "mats": [
                        {"dl": "1.doc", "title":"Mname"}, {"dl":"2.doc", "title":"Mname2"}
                    ]
                }, {
                    "id": 2,
                    "title": "On A Wednesday Night In Tokyo",
                    "preps": [
                        {"dl": "1.doc", "title":"name"}, {"dl":"2.doc", "title":"name2"}
                    ],
                    "mats": [
                        {"dl": "1.doc", "title":"Mname"}, {"dl":"2.doc", "title":"Mname2"}
                    ]
                }
            ]
        },
        {
            "id": 3,
            "title": "Dein Auftritt",
            "prefix": "Programm 3"
        },{
            "id": 4,
            "title": "Familienbande",
            "prefix": "Programm 4"
        },
        {
            "id": 5,
            "title": "Geschichte, Theorie und Praxis des Dokumentarfilms",
            "prefix": "Programm X"

        }
    ]
}, site = {
    "items": [
        {
            "title": "Das Projekt",
            "route": "das-projekt"
        },
        {
            "title": "Didaktik",
            "route": "didaktik"
        },
        {
            "title": "Unterricht",
            "route": "unterricht"
        },
        {
            "title": "Impressum",
            "route": "impressum"
        }
    ]
};