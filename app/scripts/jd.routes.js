(function ($, window, document, undefined) {
    'use strict';
    var pluginName = 'routes',
        defaults = {
            propertyName: 'value'
        };

    function Plugin(element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
        this._running = false;
    }

    Plugin.prototype = {
        init: function () {

            console.log('Hello from routes');
            var self = this;
            // bind hashchange

            $(window).on('hashchange', function () {
                if (self._running === false) self.transitionTo(location.hash.replace('#', ''));
            });

        },
        transitionTo: function (route) {
            // handle content change
            var that = this,
                routes = route.split('/');
            console.log(route);
            // MAIN-ROUTE
            if (route.search('/') < 0) {
                if (!that._running && !$('#' + route).hasClass('current')) {
                    that._running = true;
                    //location.hash = route;
                    $('.navigation-main .current').removeClass('current');
                    $('.navigation-main').find('[href="#' + route + '"]').addClass('current');
                    $('.level-2').hide();
                    switch (route) {
                        case 'das-projekt':
                            console.log('rendering programms');
                            $('.programm-list, .screening-list').children().remove();
                            tmpl.render('#programm-list-template', bunker.getProgramms(), '.programm-list');
                            tmpl.render('#prep-list-template', bunker.getProgramms(), '.screening-list');
                            $('.sub').on('click','a', function (e) {
                                e.preventDefault();
                                $('.programm-list .selected').removeClass('selected');
                                $(this).addClass('selected');
                                $(this).parent().parent().addClass('selected');
                                location.hash = $(this).attr('href');

                            });
                            $('.preps, .mats').on('click', 'a', function () {
                                    console.log("das");
                                    that.displayMessage();
                                }
                            );
                            break;
                        default:
                            console.error('no-route defined');
                            break;
                    }
                    if ($('section.current').length > 0) {
                        $('section.current').removeClass('current').fadeOut(200, function () {
                            $('#' + route).addClass('current').fadeIn(200, function () {
                                that._running = false;
                            });
                        });
                    } else {
                        $('#' + route).addClass('current').fadeIn(200, function () {
                            that._running = false;
                        });
                    }

                }
            }
            // PROGRAMM ROUTES
            else {
                if (!that.running) {
                    that._running = true;
                    var elm = '#programm-' + routes[1],
                        $elm = $(elm);
                    // PREPARATION

                    if (routes[2] === "preparation") {
                        $('.programm').hide();
                        $('.level-2').show();
                        $elm.show().find('.preps').show();
                        $elm.find('.mats').hide();

                    } else {
                        $('.programm').hide();
                        $('.level-2').show();
                        $elm.show().find('.preps').hide();
                        $elm.find('.mats').show();
                    }
                    that._running = false;

                }
            }
        },

        displayMessage: function () {
            var msg = "Die Datei wurde in ihrem Download-Ordner gespeichert.";
            $('#msg').text(msg).show();
            setTimeout(function () {
                $('#msg').fadeOut();
            }, 3000);
        }
    };
    $.fn[pluginName] = function (options) {
        this.each(function () {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
        return this;
    };

})(jQuery, window, document);