;
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['bunker'], function (is) {
            // Also create a global in case some scripts
            // that are loaded still are looking for
            // a global even when an AMD loader is in use.
            return (root.bunker = factory(is));
        });
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like enviroments that support module.exports,
        // like Node.
        module.exports = factory(require('bunker_js'));
    } else {
        // Browser globals (root is window)
        root.bunker = factory(root.bunker);
    }
}(this, function (bunker) {

    // Baseline
    /* -------------------------------------------------------------------------- */

    var root = this || global;
    var previousIs = root.jd;

    // define 'is' object and current version
    bunker = {};
    bunker.VERSION = '0.0.3';


    // define interfaces
    var _static = {};
    var _programms = {};
    var _data = null;

    bunker.setStatic = function (json) {
        _static = json;
    };
    bunker.getStatic = function () {
        return _static;
    };
    bunker.setProgramms = function (json) {
        _programms = json;


    };
    bunker.getProgramms = function () {
        return _programms;
    };
    bunker.setData = function (json) {
        _data = json;
    };
    bunker.getData = function () {
        return _data;
    };

    return bunker;

}));
;


Handlebars.registerHelper('list', function(context, options) {
    var ret = "<ul>";

    for(var i=0, j=context.length; i<j; i++) {
        ret = ret + "<li>" + options.fn(context[i]) + "</li>";
    }

    return ret + "</ul>";
});


(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['tmpl'], function (is) {
            // Also create a global in case some scripts
            // that are loaded still are looking for
            // a global even when an AMD loader is in use.
            return (root.tmpl = factory(is));
        });
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like enviroments that support module.exports,
        // like Node.
        module.exports = factory(require('tmpl_js'));
    } else {
        // Browser globals (root is window)
        root.tmpl = factory(root.tmpl);
    }
}(this, function (tmpl) {

    // Baseline
    /* -------------------------------------------------------------------------- */

    var root = this || global;
    var previousIs = root.jd;

    // define 'is' object and current version
    tmpl = {};
    tmpl.VERSION = '0.0.3';


    // define interfaces
    var _static = {};
    var _programms = {};
    var _data = null;

    tmpl.render = function (tmpl, data, destination) {

        var source = $(tmpl).html();
        var template = Handlebars.compile(source);
        var context = data;
        var html = template(context);
        $(destination).append(html);

    };
    tmpl.getStatic = function () {
        return _static;
    };
    tmpl.setProgramms = function (json) {
        _programms = json;

    };
    tmpl.getProgramms = function () {
        return _programms;
    };
    tmpl.setData = function (json) {
        _data = json;
    };
    tmpl.getData = function () {
        return _data;
    };

    return tmpl;

}));
